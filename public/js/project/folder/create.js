$(document).ready(function () {
    
    $('#createFolderSubmit').click(function() {
        var name = $('#modalName').val();
        var path = $('#modalPath').val();
        var desc = $('#modalDesc').val();
       folder.addItem(name,path,desc);
       $('#createFolderModal').modal('hide');
    });


    $('#editFolderSubmit').click (function(){
        var n_name = $('#editFolderModalName').val();
        var n_path = $('#editFolderModalPath').val();
        var n_desc = $('#editFolderModalDesc').val();
        
        var index = $('#folderHiddenId').val();
        folder.replace (n_name,n_path,n_desc, index);

    });
    


});

class Folder {
    constructor() {
        this.items = [];

        // var obj = new Object();
        // obj.name = "name";
        // obj.path  = "path";
        // obj.desc = "desc";

        // this.items.push(obj);
        this.populateTable();
    }

    replaceItem(name,path,desc, index){
        var obj = new Object();
        obj.name = name;
        obj.path  = path;
        obj.desc = desc;

        this.items[index] = obj;
        this.populateTable();
    }

    addItem(name,path,desc){
        var obj = new Object();
        obj.name = name;
        obj.path  = path;
        obj.desc = desc;

        this.items.push(obj);
        this.populateTable();
    }

    remove(itemIndex, that){
        this.items.splice(itemIndex,1);
        this.populateTable();
    }

    edit(itemIndex, that){
        
        
        $('#editFolderModalName').val(this.items[itemIndex].name);
        $('#editFolderModalPath').val(this.items[itemIndex].path);
        $('#editFolderModalDesc').val(this.items[itemIndex].desc);
        $('#folderHiddenId').val(itemIndex);
        $('#editFolderModal').modal('show');

    }


    replace(name,path, desc, index){
        var obj1 = new Object();
        obj1.name = name;
        obj1.desc = desc;
        obj1.path = path;
        this.items[index] = obj1;
        this.populateTable();
        $('#editFolderModal').modal('hide');
    }

    populateTable(){
        if (this.items.length<1) {
            var strVar = "<div class=\"well text-center\">No Folders found.</div>";
            $('#folderTable').html(strVar);
            return;
        };
        var strVar="";
        strVar += "<div class=\"table-responsive\">";
        strVar += "<table class=\"table table-responsive table-bordered table-striped datatable\">";
        strVar += "    <thead>";
        strVar += "    <th>Name<\/th>";
        strVar += "    <th>Path<\/th>";
        strVar += "    <th>Description<\/th>";
        strVar += "    <th width=\"50px\">Action<\/th>";
        strVar += "    <\/thead>";
        strVar += "    <tbody>";
        for(var item in this.items){
            strVar += "        <tr>";
            strVar += "            <td><span class=\"text-primary\">"+this.items[item].name+"<\/span><\/td>";
            strVar += "            <td>"+this.items[item].path+"<\/td>";
            strVar += "            <td>"+this.items[item].desc+"<\/td>";
            strVar += "            <td>";
            strVar += "             <a href=\"#\" onclick=\"folder.edit("+item+",this)\"><i class=\"glyphicon glyphicon-edit\"><\/i><\/a>"
            strVar += "             <a href=\"#\" onclick=\"folder.remove("+item+",this)\"><i class=\"glyphicon glyphicon-trash\"><\/i><\/a>"
            strVar += "            <\/td>";
            strVar += "        <\/tr>";
        }

        strVar += "    <\/tbody>";
        strVar += "<\/table><\/div>";

        $('#folderTable').html(strVar);

    }

}

var folder = new Folder();