<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoldersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('folders',function(Blueprint $table){
            $table->increments('id');
            $table->string('name');

            $table->string('path');
            $table->string('desc');
            $table->integer('project_id');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('folders');
    }
}
