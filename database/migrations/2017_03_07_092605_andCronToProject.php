<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AndCronToProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {

            $table->dropColumn('time_period');
            $table->dropColumn('time_picked');
            $table->dropColumn('dayName');
            $table->dropColumn('date_of_Month');
            $table->string('schedule');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('projects', function ($table) {
            $table->string('time_period');
            $table->time('time_picked');
            $table->string('dayName');
            $table->int('date_of_Month');
            $table->dropColumn('schedule');
        });
    }
}
