<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkDbase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dbases',function($table){
            $table->integer('project_id')->unsigned()->change();
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');   
        });
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dbases',function($table){
            $table->dropForeign('dbases_project_id_foreign');
        });
    }
}
