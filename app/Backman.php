<?php

namespace App;

use Illuminate\Support\Facades\Storage;
use ZipArchive;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use App\Http\Requests;
use BackupManager\Manager;
use BackupManager\Filesystems\Destination;
use \Illuminate\Support\Facades\File;
use FTP;
use App;
use Config;
use Carbon\Carbon;

class Backman
{
    public $folders = [];
    public $dbs = [];
    public $config = ['local'=>true,'ftp'=>false];
    public $cleanup = [
        'local' => 'daily',
        'ftp'   =>  'daily'
    ];
    public $ftpConnection = 'connection1';
    public $ftpStoreDirectory = 'files/';

    public $expectedLocalBackups = [];
    public $expectedFtpBackups = [];

    public function __construct() {
        $this->manager = App::make(Manager::class);
    }

    public function populateFromProject($project)
    {
        foreach ($project->folders as $key => $folder) {
            $this->addFolder([['name'=>$folder->name,'path'=>$folder->path]]);
        }

        foreach ($project->dbases as $key => $dbase) {
            $this->addDb($dbase->name);
        }
        if($project->ftp){
            $this->config['ftp'] = true;
        }
        $this->cleanup['local'] = $project->cleanup;
        $this->cleanup['ftp'] = $project->cleanup;
        
        $project->load('history');
        foreach ($project->history as $backup) {
            if($backup->ftp){
                array_push($this->expectedFtpBackups,basename($backup->path));
            }else{
                array_push($this->expectedLocalBackups,basename($backup->path)); 
            }
        }
        
    }
    
    public function setConfig($config)
    {
        if (isset($config['local'])) {
            if ($config['local']) {
                $this->config['local'] = true;
            }else{
                $this->config['local'] = false;
            }
        }
        if(isset($config['ftp'])){
            if ($config['ftp']) {
                $this->config['ftp'] = true;
            }else{
                $this->config['ftp'] = false;
            }
        }
    }

    public function setFtpConnection($connection)
    {
        $this->ftpConnection = $connection;
    }

    public function addFolder($folder)
    {
        if(is_array($folder)){
            $this->folders = array_merge($this->folders, $folder);
        }else{
            array_push($this->folders,$folder);    
        }
    }
    public function addDb($db)
    {
        if(is_array($db)){
            $this->dbs = array_merge($this->dbs, $db);
        }else{
            array_push($this->dbs,$db);    
        }
    }

    public function setTempConnection($db)
    {
        $tempCon = 'tempCon';
        config(['database.connections.tempCon.database' => $db]);
        return $tempCon;
    }


    public function restoreBackup($zipPath,$type)
    {
        if($type == 'ftp'){
             $dest = base_path('storage/app/'.basename($zipPath));
             if($this->downloadFromFtp($zipPath, $dest)){
                 $zipPath = $dest;
             }
        }
        
        //$destFolder = $this->extractBackup($zipPath);

        $destFolder = base_path('storage/app/'.basename($zipPath));

        $this->restoreDatabases($destFolder);
        $this->restoreFolders($destFolder);

    }
    public function extractBackup($zipPath)
    {
        $dest = base_path('storage/app/restore/'.basename($zipPath,'.zip'));

        $res = $this->extractZip($zipPath, $dest);
        if (!$res) {
            return false;
        }
        return $dest;
    }

    public function restoreFolders($destFolder)
    {

        foreach ($this->folders as $folder) {
           

            $folderNameWithoutExtension = $destFolder.'/'.$folder['name'];
            File::copyDirectory($folderNameWithoutExtension, $folder['path']);


        }
        
    }

    public function restoreDatabases($destFolder)
    {
        $path = pathinfo($destFolder);
        
        $extension = '.sql';
        foreach ($this->dbs as $db) {
            $fileName = $path['basename'].'/'.$db.'.sql';

            $conName = $this->setTempConnection($db);
            $this->manager = App::make(Manager::class);
            $this->manager->makeRestore()->run('local', $fileName, $conName, 'null');
        }

        
    }
    

    public function extractZip($source,$dest=null)
    {
        if (!$dest) {

            $info = pathinfo($source);
            $dest = str_replace('.'.$info['extension'],'',$source);
        }
        
        $zip = new ZipArchive;
        if ($zip->open($source) === TRUE) {
            $zip->extractTo($dest);
            $zip->close();
            return $dest;
        } 
        return false;
    }
   

    public function createBackup()
    {
        $res = [];
        $folderPath = $this->createFolder();
        $this->backupDatabase($folderPath);
        $this->backupFolders($folderPath);

//        $zipName = $this->zipBackup($folderPath);
//
       if($this->config['ftp']){
           $res['ftp'] = $this->uploadBackupToFtp($folderPath);
        }
        $res['local'] = $folderPath;
  //      return $res;
        return $res;
    }

    public function uploadBackupToFtp($filePath)
    {
        $path = pathinfo($filePath);
        $source = $this->zipBackup($filePath);
        $dest = $this->ftpStoreDirectory.$path['basename'];
        $this->uploadToFtp($source,$dest);
        File::delete($source);

        return $dest;
    }


    public function uploadToFtp($source,$dest)
    {

        return FTP::connection($this->ftpConnection)->uploadFile($source, $dest);

    }
    public function downloadFromFtp($source,$dest)
    {
        return FTP::connection($this->ftpConnection)->downloadFile($source,$dest);
    }

    public function backupDatabase($newFolderName)
    {
        foreach ($this->dbs as $db) {
            $conName = $this->setTempConnection($db);
            
            $this->manager = App::make(Manager::class);

            $dbBackupPath = basename($newFolderName).'/'.$db.'.sql';
            $this->manager->makeBackup()
                ->run($conName, [
                    new Destination('local', $dbBackupPath),
                ], 'null');
        }
    }
   

    public function zipBackup($newFolderName)
    {
        $dest = $newFolderName.'.zip';
        $this->zipData($newFolderName,$dest);

        return $dest;
    }

    public function removeDir($newFolderName)
    {
        array_map('unlink', glob("$newFolderName/*.*"));
        rmdir($newFolderName);
    }



    public function backupFolders($newFolderName)
    {
         foreach ($this->folders as $folder) {
            $sourceFolder = $folder['path'];
            if(file_exists($sourceFolder)){
                $dest = $newFolderName.'/'.$folder['name'];
//                $this->zipData($sourceFolder,$dest);
            File::copyDirectory($sourceFolder, $dest);
            }
        }
    }

    public function createFolder()
    {
        $folderName = "backup-".time();
        $folderPath = base_path('storage/app/'.$folderName); 
        mkdir($folderPath);
        return $folderPath;
    }


    public function zipData($source, $destination) {
        if (extension_loaded('zip')) {
            if (file_exists($source)) {
                $zip = new ZipArchive();
                if ($zip->open($destination, ZIPARCHIVE::CREATE)) {
                    $source = realpath($source);
                    if (is_dir($source)) {
                        $iterator = new RecursiveDirectoryIterator($source);
                        // skip dot files while iterating 
                        $iterator->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);
                        $files = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::SELF_FIRST);
                        foreach ($files as $file) {
                            $file = realpath($file);
                            if (is_dir($file)) {
                                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                            } else if (is_file($file)) {
                                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                            }
                        }
                    } else if (is_file($source)) {
                        $zip->addFromString(basename($source), file_get_contents($source));
                    }
                }
                return $zip->close();
            }
        }
        return false;
    }


    public function copyr($source, $dest)
    {
        // Check for symlinks
        if (is_link($source)) {
            return symlink(readlink($source), $dest);
        }
        
        // Simple copy for a file
        if (is_file($source)) {
            return copy($source, $dest);
        }

        // Make destination directory
        if (!is_dir($dest)) {
            mkdir($dest);
        }

        // Loop through the folder
        $dir = dir($source);
        while (false !== $entry = $dir->read()) {
            // Skip pointers
            if ($entry == '.' || $entry == '..') {
                continue;
            }

            // Deep copy directories
            $this->copyr("$source/$entry", "$dest/$entry");
        }

        // Clean up
        $dir->close();
        return true;
    }


    public function dailyBackupRemove($list)
    {
        $res = [];
        $today = Carbon::now()->subDays(1);
        $today->minute = 0;
        $today->hour = 0;
        $today->second = 0;

         foreach ($list as $single) {
            $timestamp = substr($single,-14,-4);
            $file = Carbon::createFromTimestamp($timestamp);
            if($file->lt($today)){
                array_push($res, $single);
            }
        }
        return $res;
    }
    public function weekBackupRemove($list)
    {
        $res = [];
        $relative =  Carbon::now()->startOfWeek()->subDays(7);
        $relative->minute = 0;
        $relative->hour = 0;
        $relative->second = 0;
        
        foreach ($list as $single) {
            $timestamp = substr($single,-14,-4);
            $file = Carbon::createFromTimestamp($timestamp);
            if($file->lt($relative)){
                array_push($res, $single);
            }
        }
        return $res;
    }

    public function monthlyBackupRemove($list)
    {
        $res = [];
        $relative = new Carbon('first day of last month');
        $relative->minute = 0;
        $relative->hour = 0;
        $relative->second = 0;

        foreach ($list as $single) {
            $timestamp = substr($single,-14,-4);
            $file = Carbon::createFromTimestamp($timestamp);
            if($file->lt($relative)){
                array_push($res, $single);
            }
        }
        return $res;
    }

    function startsWith($haystack, $needle) {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    function endsWith($haystack, $needle) {
        // search forward starting from end minus needle length characters
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
    }


    public function filterLocalProjectPackups($list)
    {
        $backups = [];
        foreach ($this->expectedLocalBackups as $backupName) {
            if(in_array($backupName, $list)){
                array_push($backups,$backupName);
            }
        }
        return $backups;
    }
    public function filterFtpProjectPackups($list)
    {
        $backups = [];
        foreach ($this->expectedFtpBackups as $backupName) {
            if(in_array($backupName, $list)){
                array_push($backups,$backupName);
            }
        }
        return $backups;
    }
    

    public function cleanup()
    {

        if($this->config['local']){
            $path = base_path('storage/app/');
            $list = scandir($path);

            $list = array_filter($list,function ($value)
            {
                return $this->startsWith($value,'backup') && $this->endswith($value,'.zip');
            });
            
            $list = $this->filterLocalProjectPackups($list);

            $listToDelete = [];
            switch ($this->cleanup['local']) {
                case 'monthly':
                    $listToDelete = $this->monthlyBackupRemove($list);
                    break;
                case 'weekly':
                    $listToDelete = $this->weekBackupRemove($list);
                    break;
                case 'daily':
                    $listToDelete = $this->dailyBackupRemove($list);
                    break;
            }
            $this->localDeleteFiles($listToDelete,base_path('storage/app/'));
        }
        
         if($this->config['ftp']){
            $connection = FTP::connection($this->ftpConnection);
            $connection->changeDir($this->ftpStoreDirectory) ;
            
            $list = $connection->getDirListing();

            $list = $this->filterFtpProjectPackups($list);

            $listToDelete = [];
            switch ($this->cleanup['ftp']) {
                case 'monthly':
                    $listToDelete = $this->monthlyBackupRemove($list);
                    break;
                case 'weekly':
                    $listToDelete = $this->weekBackupRemove($list);
                    break;
                case 'daily':
                    $listToDelete = $this->dailyBackupRemove($list);
                    break;
            }
            $this->ftpDeleteFiles($listToDelete);
              
        }

    }

    public function localDeleteFiles($list,$directory = null)
    {
        foreach ($list as $file) {
            if ($directory) {
                $file = $directory . $file;
            }
            
            if(File::exists($file)){
                File::delete($file);
            }
        }
        return true;
    }
    public function ftpDeleteFiles($list)
    {
        $connection = FTP::connection($this->ftpConnection);
        $connection->changeDir($this->ftpStoreDirectory) ;

        foreach ($list as $file) {
           $connection->delete($file);
        }
        FTP::disconnect($this->ftpConnection);
        return true;
    }

    

}
