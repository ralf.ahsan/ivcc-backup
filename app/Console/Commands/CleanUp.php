<?php

namespace App\Console\Commands;

use App\ProjectBackup;
use Cron\CronExpression;
use Illuminate\Console\Command;
use \Illuminate\Support\Facades\File;
use Log;
use App\Project;
use Storage;

use Carbon\Carbon;
use DateTime;
use App\Backman;

class CleanUp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geekinn:cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $projects = Project::where('status', '=', 'pending')->with('folders', 'dbases')->get();

        foreach ($projects as $project) {
            $cron = CronExpression::factory($project->cleanup);
            if ($cron->isDue()) {
                $this->cleanUp($project->id, $project->cleanup);
            }

        }

    }

    public function cleanUp($id, $str)
    {
        $date = new DateTime();
        Log::info('CleanUp of project id ' . $id . ' at ' . $date->format('Y-m-d H:i:s'));

        $cron = CronExpression::factory($str);

        $histories = ProjectBackup::where('created_at', '<', $cron->getPreviousRunDate())->where('project_id', '=', $id)->get();

        foreach ($histories as $history) {
           File::deleteDirectory($history->path);
           ProjectBackup::where('id', '=', $history->id)->delete();
        }

    }


}
