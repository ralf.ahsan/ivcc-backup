<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Log;
use App\Project;

use Cron\CronExpression;

use DateTime;
use Carbon\Carbon;

use App\Backman;
use App\ProjectBackup;

class TakeBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geekinn:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        
        
        // $projectId = $this->argument('project');
        // $this->info('Display this on the screen');
        // $this->error('Something went wrong!');
        
        $current = Carbon::now();
        $current->second = 0;
        $projects = Project::where('status','=','pending')->with('folders','dbases')->get();


        foreach ($projects as $project) {


            $projects = Project::where('status', '=', 'pending')->with('folders', 'dbases')->get();

            foreach ($projects as $project) {
                $cron = CronExpression::factory($project->schedule);
                if ($cron->isDue()) {
                    $this->backupNow($project->id);
                }

            }

            
        }
    }

    public function backupNow($project_id)
    {
        $project = Project::where('id', '=', $project_id)->with('folders', 'dbases')->first();
        $date = new DateTime();
        Log::info('Took Backup of project id ' . $project->id . ' at ' . $date->format('Y-m-d H:i:s'));

        $backman = new Backman();
        $backman->populateFromProject($project);
        $path = $backman->createBackup();

        $projectBackup = new ProjectBackup();
        $projectBackup->project_id = $project->id;
        $projectBackup->path = $path['local'];
        $projectBackup->ftp = false;
        $projectBackup->save();

        if ($project->ftp) {
            $projectFtpBackup = new ProjectBackup();
            $projectFtpBackup->project_id = $project->id;
            $projectFtpBackup->path = $path['ftp'];
            $projectFtpBackup->ftp = true;
            $projectFtpBackup->save();
        }
    }
}
