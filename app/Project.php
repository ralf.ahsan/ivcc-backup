<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
	
    
    public function folders()
    {
        return $this->hasMany('App\Folder');
    }
   
    public function dbases()
    {
        return $this->hasMany('App\Dbase');
    }
    public function history()
    {
        return $this->hasMany('App\ProjectBackup');
    }
}
