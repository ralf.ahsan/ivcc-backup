<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => 'auth'], function () {

    Route::get('/',function ()
    {
        return redirect()->route('project.index');
    });


    Route::get('/backup',['uses'=>'BackupController@backup','as'=>'db.backup']);
    Route::get('/restore',['uses'=>'BackupController@restore','as'=>'db.restore']);

    Route::resource('project', 'ProjectController');

    Route::get('project/history','ProjectController@history');



    Route::get('/check','FolderController@check');

    // Route::get('/project_test','ProjectController@test');

    Route::get('/project/{id}/history',['uses'=>'ProjectController@history','as'=>'project.history']);
    Route::get('/projectbackup/{id}/restore',['uses'=>'ProjectController@restore','as'=>'project.backup.restore']);
    Route::get('/projectbackup/{id}/backup',['uses'=>'ProjectController@takeBackup','as'=>'project.backup.now']);
    // Route::get('/project_schedule','ProjectController@scheduleDaily');

    // Route::get('/test', ['uses'=>'DbaseController@index','as'=>'check.db']);

    // Route::get('/checkftp','BackupController@ftpCheck');

    // Route::get('/cleanup','ProjectController@cleanup');

});