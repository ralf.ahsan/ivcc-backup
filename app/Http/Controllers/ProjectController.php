<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProjectCreateRequest;

use View;
use App\Http\Requests;
use App\Project;
use App\Folder;
use App\Dbase;

use App\ProjectBackup;
use Carbon\Carbon;
use DateTime;
use App\Backman;

use Log;
use File;
use FTP;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $projects = Project::with('folders', 'dbases')->orderBy('created_at', 'asc')->get()->toArray();

        foreach ($projects as $key => $project) {
            $projects[$key]['folderNames'] = implode(', ', collect($project['folders'])->lists('name')->toArray());
            $projects[$key]['dbaseNames'] = implode(', ', collect($project['dbases'])->lists('name')->toArray());
        }

        return view('projects.index', [
            'projects' => $projects]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projects.create');
    }

    public function test(Request $req)
    {
        dd('asdsa');


        return $data;


        // $dbase = new Dbase();
        // $dbase->name = new string();
        // $dbase->desc = new string();

        // $project->dbases()->save($dbase);


        // dd($dbase);

        // $p1 = Project::find(5);
        // dd($p1->dbases);

        // $db = Dbase::find(2);
        // dd($db->project);

        // $folder = new Folder();
        // $folder->name = new string();
        //  $folder->path = new string();
        //   $folder->desc = new string();

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectCreateRequest $request)
    {
        $data = $request->all();
        $project = new Project();
        $project->name = $data['Name'];
        $project->schedule = $data['schedule'];
        $project->cleanup = $data['cleanup'];


        if (isset($data['check_this'])) {
            $project->ftp = true;
        } else {
            $project->ftp = false;
        }

        $project->save();

        if (isset($data['folderName'])) {
            for ($i = 0; $i < count($data['folderName']); $i++) {
                $folder = new Folder();
                $folder->name = $data['folderName'][$i];
                $folder->path = $data['folderPath'][$i];
                $folder->desc = $data['folderDesc'][$i];
                $project->folders()->save($folder);
            }
        }
        if (isset($data['dbaseName'])) {
            for ($i = 0; $i < count($data['dbaseName']); $i++) {
                $dbase = new Dbase();
                $dbase->name = $data['dbaseName'][$i];

                $dbase->desc = $data['dbaseDesc'][$i];
                $project->dbases()->save($dbase);

            }
        }

        return redirect('/project');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('projects.history');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $project = Project::find($id)->with('folders', 'dbases')->orderBy('created_at', 'asc')->first();
        $folderJson = json_encode($project->folders);
        return view('projects.edit', [
            'project' => $project, 'folderJson' => $folderJson]);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $project = Project::findOrFail($id);

        $this->validate($request, [
            'name' => 'required',
            'time_period' => 'required',
            'time_picked' => 'required',
            'dayName' => 'required',
            'date_of_Month' => 'required'
        ]);

        for ($i = 0; $i < count(['folderName']); $i++) {
            $folder = Folder::findOrFail($id);
            $this->validate($request, [
                'name' => 'required',
                'path' => 'required',
                'desc' => 'required'

            ]);

            $project->folders()->save($folder);
        }
        for ($i = 0; $i < count(['dbaseName']); $i++) {
            $folder = Folder::findOrFail($id);
            $this->validate($request, [
                'name' => 'required',
                'path' => 'required',
                'desc' => 'required'

            ]);

            $project->dbases()->save($folder);
        }


        $input = $request->all();

        $project->fill($input)->save();

        Session::flash('flash_message', 'Project successfully added!');

        return redirect()->back();


    }

    public function history($id)
    {
        $project = Project::where('id', $id)->with(['history', 'dbases', 'folders'])->first();
        return view('projects.history', ['project' => $project]);
    }


    public function restore($id)
    {
        $projectBackup = ProjectBackup::find($id);
        $project = $projectBackup->project;
        $backman = new Backman();
        $backman->populateFromProject($project);
        if ($projectBackup->ftp) {
            $backman->restoreBackup($projectBackup->path, 'ftp');
        } else {
            $backman->restoreBackup($projectBackup->path, 'local');
        }
        return redirect()->route('project.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        // dd($id);

        $project = Project::where('id', '=', $id)->with('dbases', 'folders')->first();

        foreach ($project->dbases as $key => $value) {
            $value->delete();
        }

        foreach ($project->folders as $key => $value) {
            $value->delete();
        }

        $project->delete();
        return redirect('project');

    }

    // public function scheduleDaily()
    // {
    //    $current = Carbon::now();
    //    $current->second = 0;
    //    $project = Project::find(6);
    // //    $this->takeBackup($project);
    // }

    // public function takeBackup($project)
    // {

    //     $backman = new Backman();
    //     $backman->populateFromProject($project);
    //     // dd($backman);
    //     $path = $backman->createBackup();
    //     dd($path);
    // }



    public function takeBackup($project_id)
    {
        $project = Project::where('id', '=', $project_id)->with('folders', 'dbases')->first();
        $date = new DateTime();
        Log::info('Took Backup of project id ' . $project->id . ' at ' . $date->format('Y-m-d H:i:s'));

        $backman = new Backman();
        $backman->populateFromProject($project);
        $path = $backman->createBackup();

        $projectBackup = new ProjectBackup();
        $projectBackup->project_id = $project->id;
        $projectBackup->path = $path['local'];
        $projectBackup->ftp = false;
        $projectBackup->save();

        if ($project->ftp) {
            $projectFtpBackup = new ProjectBackup();
            $projectFtpBackup->project_id = $project->id;
            $projectFtpBackup->path = $path['ftp'];
            $projectFtpBackup->ftp = true;
            $projectFtpBackup->save();
        }
        return redirect()->route('project.index');
    }

}
