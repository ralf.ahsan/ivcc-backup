@extends('layouts.app')

@section('htmlheader_title')
  Project Index
@endsection

@section('contentheader_title')
  
@endsection


@section('main-content')

  <div class="col-md-12">
    <div class="box box-primary">
        
            <div class="box-header with-border">
              <div class="col-md-6 text-left">
                <h3 class="box-title">Projects</h3>
              </div>
              <div class="col-md-6 text-right">
                <a class="btn btn-primary btn-sm" href="{{route('project.create')}}">Create New</a>
              </div>

            </div>

                <div class="box-body">
                      @if (count($projects) > 0)
                          
                           <table class="table table-striped task-table">

                    
                        <thead>
                            <th>Project Name</th>
                            <th>Schedule</th>


                            <th>Folders</th>
                            <th>Databases</th>
                            <th>Ftp</th>
                            <th>Cleanups</th>
                            <th>Action</th>
                            
                        </thead>

                    <tbody>
                        @foreach ($projects as $project)
                            <tr>
                                
                                <td class="table-text">
                                  <a href="{{route('project.history',$project['id'])}}"><div>{{ $project['name'] }}</div></a>
                                    
                                    
                                </td>
                                <td>
                                  <div>{{ $project['schedule'] }}</div>
                                </td>

                                 <td>

                                    {{$project['folderNames']}}

                                </td>

                                <td>
                                    {{$project['dbaseNames']}}
                                </td>
                                <td>
                                  @if($project['ftp'])
                                    <span class="label label-success">Yes</span>
                                  @else 
                                    <span class="label label-primary">No</span>
                                  @endif
                                </td>
                                <td>{{ucfirst($project['cleanup'])}}</td>
                                <td>

                                  <!-- <div><i class="glyphicon glyphicon-trash" ></i>
                                  <i class="glyphicon glyphicon-edit" ></i></div> -->

                                      {!! Form::open(['route' => ['project.destroy', $project['id']], 'method' => 'delete']) !!}
                                        {{ Form::submit('Delete',['class'=>'btn btn-xs btn-danger']) }}
                                      {!! Form::close() !!}

                                      <a class="btn btn-primary btn-xs" href="{{route('project.backup.now',$project['id'])}}">Backup</a>

                                      {{-- {!! Form::open(['route' => ['project.edit', $project['id']], 'method' => 'get']) !!}
                                        {{ Form::submit('edit',['class'=>'btn btn-xs btn-primary']) }}
                                      {!! Form::close() !!} --}}


                                </td>
                                

                                
                            </tr>
                        @endforeach
                         </tbody>
                </table>
            
    @endif
    </div>
    </div>
    </div>
@endsection

@push('js')
<script type="text/javascript">

jQuery(document).ready(function($) {


});

</script>
@endpush
               

