@extends('layouts.app')

@section('htmlheader_title')
	Project Create
@endsection

@section('contentheader_title')
  This is sample content
@endsection


@section('main-content')
 <form action="{{route('project.store')}}" id="formSubmit" method="post">



{{ Form::model($project, ['action'  => ['ProjectController@edit', $project->id], 'role'=> 'form']) }}
  {{ csrf_field() }}

	<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Project</h3>
            </div>

                <div class="box-body">
                  <div class="form-group col-md-4" >
                    <label for="exampleInputEmail1">Name</label>
                    <input name="Name" type="text" value="{{$project->name}}" class="form-control"  placeholder="Name">
                  </div>
                 

                  <div class="form-group col-md-4">
                    <label for="sel1">Select Time-Period:</label>
                    <select name="repeat" class="form-control" id="repeat">
                      <option name="daily" value="daily">Daily</option>
                      <option name="weekly" value="weekly">Weekly</option>
                      <option name="monthly" value="monthly">Monthly</option>
                    </select>
                  </div>

                  

                  <div class="form-group col-md-4" id ="picker">
                    <label >Time picker:</label>

                    <div class="input-group bootstrap-timepicker timepicker">
                      <input name="timePicked" id="timepicker1" type="text" class="form-control input-small">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                    </div>
                  </div>

                  

                  <div class="form-group col-md-4" id ="day-picker">
                    <label for="sel1">Select Day:</label>
                    <select name="dayName" class="form-control" id="repeat">
                      <option name="sunday" value="sunday"> Sunday</option>
                      <option name="monday" value="monday"> Monday</option>
                      <option name="tuesday" value="tuesday"> Tuesday</option>
                      <option name="wednesday" value="wednesday"> Wednesday</option>
                      <option name="thursday" value="thursday"> Thursday</option>
                      <option name="friday" value="friday"> Friday</option>
                      <option name="saturday" value="saturday"> Saturday</option>
                    </select>
                  </div>

                 

                   <div class="form-group col-md-4" id ="date-picker">
                    <label for="sel1">Select Date:</label>
                    <select name="date" class="form-control" id="repeat">
                      @for($i=1;$i<=30;$i++)
                        <option >{{$i}}</option>
                      @endfor
                    </select>
                  </div>



                </div>
                <div align="left" class="box-footer"></div>
                @include('projects.databases')
                @include('projects.folders')


      </div>
      @include('projects.editCleanup')
      
      
      
  	</div>
  
  <div class="clearfix"></div>



</form>
@include('projects.modals.folder.create')
@include('projects.modals.folder.edit')
@include('projects.modals.dbase.create')
@include('projects.modals.dbase.edit')

@endsection




@push('js')
   <script src="{{asset('js/project/folder/create.js')}}" type="text/javascript"></script>
<!-- endpush

<!-- push(js') -->
<script src="{{asset('js/project/dbase/create.js')}}" type="text/javascript"></script>
<script src="{{asset('plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>

<!-- <script src="{{asset('public/plugins/datepicker/bootstrap-datepicker.js')}}"></script> -->



<script type="text/javascript">

  retrievedFolders = jQuery.parseJSON('{!! $folderJson !!}');

  $('#timepicker1').timepicker({
    showInputs: false
  });

  jQuery(document).ready(function($) {
    
    $('#day-picker').hide();
    $('#date-picker').hide();
  
    $('#repeat').change(function(event) {
      
      if($(this).val()==='daily'){
        $('#picker').show();
        $('#day-picker').hide();
        $('#date-picker').hide();

      }
      if($(this).val()==='weekly'){
        $('#day-picker').show();
        $('#picker').show();
        $('#date-picker').hide();

      }
      if($(this).val()==='monthly'){
        $('#day-picker').hide();
        $('#picker').show();
        $('#date-picker').show();

      }



    });
  $( "#formSubmit" ).submit(function( event ) {

      

    $.each(folder.items, function(i,param){
        $('<input />').attr('type', 'hidden')
            .attr('name', 'folderName[]')
            .attr('value', param.name)
            .appendTo('#formSubmit');

            $('<input />').attr('type', 'hidden')
            .attr('name', 'folderPath[]')
            .attr('value', param.path)
            .appendTo('#formSubmit');

            $('<input />').attr('type', 'hidden')
            .attr('name', 'folderDesc[]')
            .attr('value', param.desc)
            .appendTo('#formSubmit');
      });
    $.each(db.items, function(i,param){
        $('<input />').attr('type', 'hidden')
              .attr('name', 'dbaseName[]')
              .attr('value', param.name)
              .appendTo('#formSubmit');


              $('<input />').attr('type', 'hidden')
                .attr('name', 'dbaseDesc[]')
                .attr('value', param.desc)
                .appendTo('#formSubmit');
      });

     




     // debugger;
    // alert( "Handler for .submit() called." );
    // event.preventDefault();
  });




    // $('#repeat').click(function(event) {
    //   if($('#repeat').val()==='daily'){
    //     $('#picker').show();
    //     $('#day-picker').hide();
    //     $('#date-picker').hide();

    //   }
    //   if($('#repeat').val()==='weekly'){
    //     $('#day-picker').show();
    //     $('#date-picker').hide();
        
    //   }

    //   if($('#repeat').val()==='monthly'){
    //     $('#date-picker').show();

    //   }
    // });

    

  });
  
</script>
@endpush
@push('css')
<link rel="stylesheet" href="{{asset('plugins/timepicker/bootstrap-timepicker.min.css')}}">
@endpush