{{-- Database --}}
<div class="box box-primary">
    <div class="box-header with-border">
        <div class="col-md-6 text-left">
            <h3 class="box-title">Databases</h3>
        </div>
        <div class="col-md-6 text-right">
            <button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#createDbaseModal">Add
                Database
            </button>
        </div>

    </div>
    <!-- /.box-header -->

    <!-- form start -->
    <div class="box-body">
        <br>

        <div id="dbaseTable"></div>
    </div>
    <!--/testing-->
</div>
