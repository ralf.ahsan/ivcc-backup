<div class="example-modal"  >
  <div class="modal modal-primary fade" id="editDbaseModal" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Edit</h4>
        </div>
        <div class="modal-body">
            <div class="form-group ">
                <label for="name">Name : </label>
                <input type="text" id="editDbModalName" name="name" class="form-control"  placeholder="Name">
            </div>
            <input type="hidden" name="itemId" id="DbhiddenId">
            <div class="form-group ">
                <label for="desc">Description : </label>
                <textarea id="editDbModalDesc" placeholder="add some description" name="desc" class="form-control"></textarea>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
          <button type="button" id="editDbSubmit" class="btn btn-outline">Add</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
</div>