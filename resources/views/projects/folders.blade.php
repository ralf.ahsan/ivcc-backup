{{-- Folder --}}
<div class="box box-primary">
    <div class="box-header with-border">
        <div class="col-md-6 text-left">
            <h3 class="box-title">Folder</h3>
        </div>
        <div class="col-md-6 text-right">
            <button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#createFolderModal">
                Add Folder
            </button>
        </div>

    </div>
</div>
<!-- /.box-header -->
<div class="box-body">


    <div id="folderTable"></div>


</div>
<!--/testing-->
</div>
