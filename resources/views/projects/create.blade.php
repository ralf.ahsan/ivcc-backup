@extends('layouts.app')

@section('htmlheader_title')
    Project Create
@endsection

@section('contentheader_title')
    This is sample content
@endsection


@section('main-content')
    <form action="{{route('project.store')}}" id="formSubmit" method="post">
        {{ csrf_field() }}


        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Create Project</h3>
                </div>

                <div class="box-body">
                    @if ($errors->has())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Name</label>
                        <input name="Name" type="text" class="form-control" placeholder="Name">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Schedule</label>
                        <input name="schedule" type="text" class="form-control" placeholder="* * * * * *">
                    </div>


                    <div class="form-group col-md-4" id="checkFtp">
                        <label>
                            <input type="checkbox" name="check_this">FTP
                        </label>
                    </div>


                </div>
                <div align="left" class="box-footer"></div>
                @include('projects.databases')
                @include('projects.folders')
                @include('projects.cleanup')

            </div>


        </div>

        <div class="clearfix"></div>


    </form>
    @include('projects.modals.folder.create')
    @include('projects.modals.folder.edit')
    @include('projects.modals.dbase.create')
    @include('projects.modals.dbase.edit')

@endsection




@push('js')
<script src="{{asset('js/project/folder/create.js')}}" type="text/javascript"></script>
<script src="{{asset('js/project/dbase/create.js')}}" type="text/javascript"></script>



<script type="text/javascript">

    jQuery(document).ready(function ($) {



        $("#formSubmit").submit(function (event) {


            $.each(folder.items, function (i, param) {
                $('<input />').attr('type', 'hidden')
                    .attr('name', 'folderName[]')
                    .attr('value', param.name)
                    .appendTo('#formSubmit');

                $('<input />').attr('type', 'hidden')
                    .attr('name', 'folderPath[]')
                    .attr('value', param.path)
                    .appendTo('#formSubmit');

                $('<input />').attr('type', 'hidden')
                    .attr('name', 'folderDesc[]')
                    .attr('value', param.desc)
                    .appendTo('#formSubmit');
            });
            $.each(db.items, function (i, param) {
                $('<input />').attr('type', 'hidden')
                    .attr('name', 'dbaseName[]')
                    .attr('value', param.name)
                    .appendTo('#formSubmit');


                $('<input />').attr('type', 'hidden')
                    .attr('name', 'dbaseDesc[]')
                    .attr('value', param.desc)
                    .appendTo('#formSubmit');
            });

        });


    });

</script>
@endpush
@push('css')
<link rel="stylesheet" href="{{asset('plugins/timepicker/bootstrap-timepicker.min.css')}}">
@endpush